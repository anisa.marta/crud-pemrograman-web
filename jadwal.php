<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>SIM DOSEN</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="style.css">

</head>
<body>
<div class="container">
        <div class="row justify-content-center">
            <div class="col-12" style="border-style: outset;">
            <div class="header">
              <h2 style="font-weight:bold;text-shadow: 2px 2px 5px gray;">SISTEM INFORMASI PENJADWALAN DOSEN</h2>
              <h3 style="font-weight:bold;text-shadow: 2px 2px 5px gray;">UNIVERSITAS PENDIDIKAN GANESHA</h3>
            </div>
            <nav class="navbar navbar-expand-lg navbar-dark bg-primary";>
            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
              <div class="navbar-nav">

              <a class="nav-item nav-link text-white" href="home.php">Home</a>
                <a class="nav-item nav-link text-white" href="dosen.php">Dosen</a>
                <a class="nav-item nav-link text-white" href="kelas.php">Kelas</a>
                <a class="nav-item nav-link text-white" href="jadwal.php">Jadwal</a>

              </div>
            </div>
            </nav><br>
            <h4 align="center">Data Jadwal Kelas</h4><hr>
            

            <!-- Awal Card Tabel -->
	<div class="card mt-3">
	  <div class="card-body">
    <a class="btn btn-success" href="tambahjadwal.php" role="button">Tambah</a><br><br>
	    <table class="table table-bordered table-striped">
      <thead class="table-primary">
	    	<tr>
	    		<th>No.</th>
	    		<th>ID Dosen</th>
	    		<th>ID Kelas</th>
	    		<th>Jadwal</th>
	    		<th>Mata Kuliah</th>
	    		<th>Aksi</th>
	    	</tr>
        </thead>
	    	
	    </table>

	  </div>
	</div>
	<!-- Akhir Card Tabel -->
            </div>
        </div>
    </div>
    
</body>
</html>